import aiohttp
import asyncio
import logging
import coloredlogs
coloredlogs.install()

async def fetch_urls_xml(logger):
    urls = [f"https://search.wdoms.org/?sSchoolName={medschool}&iPageNumber=1" for medschool in
               ['Basel', 'Zürich', 'London']]

    urls = [[url, {}] for url in urls]

    json_ = {
        'urls':urls,
        'method':'get',
        'output': 'xml',
    }

    logger.info(f'Sending data: {json_}\n')

    async with aiohttp.ClientSession() as session:
        async with session.post('http://127.0.0.1:8000/fetch', json=json_) as resp:
            logger.info(await resp.json())


async def fetch_urls_json(logger):
    urls = [f"https://nominatim.openstreetmap.org/search/Credit%20Suisse,{zipcode},Switzerland?osm_type=N&format=json"
               for zipcode in range(2095, 2099)]

    urls = [[url, {}] for url in urls]

    json_ = {
        'urls':urls,
        'method':'get',
        'output': 'json',
    }

    logger.info(f'Sending data: {json_}\n')

    async with aiohttp.ClientSession() as session:
        async with session.post('http://127.0.0.1:8000/fetch', json=json_) as resp:
            logger.info(await resp.json())

if __name__ == '__main__':
    logger = logging.getLogger('client')
    logger.setLevel(logging.DEBUG)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(fetch_urls_xml(logger))
    loop.run_until_complete(fetch_urls_json(logger))