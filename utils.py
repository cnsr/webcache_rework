SUPPORTED_METHODS = ['GET', 'POST',]
SUPPORTED_OUTPUTS = ['xml', 'json',]
VALID_METHODS = ['GET', 'POST', 'DELETE', 'HEAD', 'PUT', 'OPTIONS', 'CONNECT', 'TRACE', 'PATCH',]

def is_valid_method(method, methods=VALID_METHODS):
    return method.upper() in methods

def is_supported(method):
    return is_valid_method(method, SUPPORTED_METHODS)

def is_supported_output(output):
    return output.lower() in SUPPORTED_OUTPUTS

def get_supported():
    return ', '.join(SUPPORTED_METHODS)