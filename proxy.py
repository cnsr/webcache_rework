import random
import ipaddress

class ProxyHandler:
    def __init__(self, db, logger):
        # db instance
        self.db = db
        # number of processed proxies per insert
        self.processed = 0
        self.logger = logger

    @property
    async def count(self):
        return await self.db.proxies.count_documents({})
        
    @property
    async def working(self):
        return await self.db.proxies.count_documents({'works': True})

    async def get_proxies(self, amount=100, params={'works': True}):
        cursor = self.db.proxies.find({**params}, {'_id': False}).sort([('score', -1)]).limit(amount * 10)
        result = await cursor.to_list(None)
        random.shuffle(result)
        return result[:amount]

    async def add_proxy(self, proxy):
        if self.validate_ip(proxy.get('address')):
            # skip if exists
            if not await self.db.proxies.find_one(
                {'address': proxy.get('address')}
            ):
                await self.db.proxies.insert_one(
                    {
                        'address': proxy.get('address'),
                        'works': True, # asuming proxy works by default
                        'port': proxy.get('port', 80),
                        'jobs_completed': 0,
                        'jobs_total': 0,
                        'score': 0.0,
                        'time': None,
                    }
                )
                self.processed += 1

    async def batch_add(self, proxies):
        # reset counter
        self.processed = 0
        for proxy_raw in proxies:
            # split into address - port pair if needed 
            if ':' in proxy_raw:
                try:
                    addr, port = proxy_raw.split(':')
                except ValueError:
                    proxy_raw = proxy_raw.split(':')
                    addr = proxy_raw[0].replace('https://', 'http://')
                    # maybe a better solution would be to use different index
                    port = proxy_raw[-1]
                except:
                    pass
                await self.add_proxy({
                    'address': addr,
                    'port': int(port) if port.isdigit() else 80
                })
            else:
                await self.add_proxy({
                    'address': proxy_raw,
                    'port': 80
                })
        self.logger.info(f'Added {self.processed} new proxies')
        return self.processed

    def validate_ip(self, addr):
        try:
            ip = ipaddress.ip_address(addr)
            return True
        except:
            return False

    async def feedback(self, addr, was_successful, is_down=False, time=None):
        proxy = await self.db.proxies.find_one({'address': addr})
        if proxy:
            proxy['jobs_total'] += 1
            if was_successful:
                proxy['jobs_completed'] += 1
            if is_down:
                proxy['works'] = False
            if time:
                try:
                    proxy['time'] = (proxy.get('time') + time) / 2
                except:
                    proxy['time'] = time
            proxy['score'] = proxy['jobs_completed'] / proxy['jobs_total']
            await self.db.proxies.update_one({'address': addr}, {'$set': proxy}, upsert=True)

    @property
    async def random(self):
        cursor = await self.db.proxies.aggregate([{
            '$match': {'works': True},
            '$sample': {'size': 1}
        }])
        result = await cursor.to_list(None)
        return result.pop()