import asyncio
import bz2
import json
import pickle
import time
from math import ceil
from datetime import datetime, timedelta

import aiohttp
from aiohttp.client_exceptions import *
from bs4 import BeautifulSoup
from validator_collection import validators
from validator_collection.errors import InvalidURLError


class DataHandler:
    def __init__(self, db, urls, method, output, max_age_days, category, proxy, headers, cookies, logger, timeout=5):
        # clean up invalid urls
        self.db = db
        self.chunk_size = 2048
        self.max_size = 5e6
        self.urls = [url_tuple for url_tuple in urls if self.validate(url_tuple[0])]
        self.method = method
        self.output = output
        self.max_age_days = max_age_days
        self.category = category
        self.proxy = proxy
        self.timeout = timeout
        self.successful = []
        self.failed = []
        self.invalid = []
        self.default_headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            'User-Agent': "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7;en-us) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Safari/530.17"
        }
        self.tasks = []
        self.default_cookies = {}
        self.default_headers.update(headers)
        self.tries = 0
        self.max_retries = 3
        self.unprocessed = []
        self.results = []
        self.logger = logger
    
    async def post_init(self):
        '''
        Can't call async functions in magic methods
        '''
        self.urls = [url for url in self.urls if url not in self.successful and url not in self.invalid]
        self.proxies = await self.proxy.get_proxies(len(self.urls))
        await self.distibute_proxies(self.proxies)

    def chunks(lst, n):
        '''
        Split list into n-sized chunks
        '''
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    async def distibute_proxies(self, working_proxies=None):
        '''
        Distributes urls over list of supplied proxies in equal chunks
        If there are not enough proxies, splits urls into equally sized chunks
        :param working_proxies: list of proxies to distribute urls to
        '''
        if not working_proxies:
            working_proxies = self.proxies

        # start by clearing unprocessed
        # for proxy in self.proxies
        self.unprocessed = []
        if len(working_proxies) != len(self.urls):
            # split into equal chunks
            distributed = list(self.chunks(self.urls, ceil(len(self.urls) / len(working_proxies))))
            self.unprocessed = list(zip(working_proxies, distributed))
        else:
            self.unprocessed = list(zip(working_proxies, self.urls))

    def process(self):
        '''
        Wrap the actual process function into a loop
        '''

        if not self.proxies:
            return {
                'status': 'no working proxies'
            }, 404

        try:
            loop = asyncio.get_event_loop()
            future = asyncio.ensure_future(self._process())
            loop.run_until_complete(future)
            loop.close()
        except:
            self.logger.critical('Asyncio event loop failure')
            return {'error': 'Processing failed'}
        if loop.is_closed():
            return {
                'successful': self.successful,
                'failed': self.failed,
                'invalid': self.invalid,
                'result': self.results,
            }, 200

    async def _process(self):
        '''
        Process urls according to proxies they are assigned
        '''
        if self.tries != 0:
            await self.post_init()
        self.logger.info("Started processing")
        for proxy, url_tuple in self.unprocessed:
            task = asyncio.ensure_future(self.get_url(url_tuple, proxy))
            self.tasks.append(task)
        
        responses = await asyncio.gather(*self.tasks)
        
        self.tries += 1

        if self.tries == self.max_retries or not self.failed:
            self.logger.info("Finished processing")
            return {
                'successful': self.successful,
                'failed': self.failed,
                'invalid': self.invalid,
                'result': self.results,
            }, 200
        else:
            result = await self._process()
            return result

    async def get_url(self, url_tuple, proxy):
        '''
        Fetch url with supplied parameters and proxy
        '''
        failed = False
        failed_connection = False
        # urls might have additinal parameters supplied in their configs
        url, config, *_ = url_tuple
        exists = await self.db.webdata.find_one({'url': url, 'expires_at': {'$lt': datetime.now()}}, {'_id': False})
        if exists:
            self.results.append(exists)
            return exists
        proxy_address = ':'.join([proxy['address'], str(proxy['port'])]).replace('https://', 'http://')
        if not proxy_address.startswith('http'):
            proxy_address = 'http://' + proxy_address
        method = config.get('method', self.method)
        headers = self.default_headers.copy()
        headers.update(config.get('headers', {}))
        cookies = config.get('cookies', {})
        post_data = config.get('data', {})
        output = config.get('output', self.output)
        start = time.time()
        data = b''
        async with aiohttp.ClientSession() as session:
            try:
                timeout = aiohttp.ClientTimeout(total=self.timeout)
                async with session.request(
                    method=method, url=url, cookies=cookies,
                    headers=headers, timeout=timeout,
                    verify_ssl=False,
                    proxy=proxy_address, json=post_data,
                    chunked=True # stream response
                ) as response:
                    while True:
                        if len(data) < self.max_size:
                            chunk = await response.content.read(self.chunk_size)
                            if not chunk:
                                break
                            data += chunk
                        else:
                            self.invalid.append(url)
            except (TimeoutError, ClientProxyConnectionError, ServerTimeoutError) as e:
                self.logger.error(f'Proxy {proxy_address} failed')
                # proxy threw an error
                failed = True
                failed_connection = True
            except Exception as e:
                self.logger.error(f'Url {url} processing failed')
                # failed to parse url
                failed = True

        self.logger.debug(f'accessing url {url} using {proxy_address}')
        if data:
            result = await self.process_result(data, output, url, proxy_address)
        else:
            self.logger.error('Didnt fetch any data')
        end = time.time() - start

        # feedback about proxy result for database
        if failed:
            await self.proxy.feedback(proxy['address'], False, failed_connection, end)
            self.failed.append(url_tuple)
        else:
            await self.proxy.feedback(proxy['address'], True, False, end)
            self.successful.append(url_tuple)

    def validate(self, url: str):
        url.replace(' ', '%20')
        try:
            value = validators.url(url, allow_special_ips=True)
            return True
        except InvalidURLError as e:
            self.invalid.append(url)
            return False

    async def process_result(self, data, output, url, proxy_address):
        try:
            interpreter = json.loads(data.decode()) if output.lower() == "json" else BeautifulSoup(data, "lxml")
            result = {
                'url': url,
                'proxy_used': proxy_address,
                'data': interpreter,
                'created_at': datetime.now(),
                'expires_at': datetime.bow() + timedelta(days=self.max_age_days),
                'format': output,
            }
            self.results.append(result)
            await self.db.webdata.insert_one(result)
            return result
        except Exception as e:
            self.logger.error('Data processing failed')
