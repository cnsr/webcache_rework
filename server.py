from aiohttp import web
import motor.motor_asyncio

from proxy import ProxyHandler
from data import DataHandler
import utils
import logging
import coloredlogs

db = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017).webcache

routes = web.RouteTableDef()

coloredlogs.install()
logger = logging.getLogger('data_handler')
logger.setLevel(logging.DEBUG)

proxy = ProxyHandler(db, logger)


@routes.post('/fetch')
async def fetch_urls(request):
    body = await request.json()

    urls = body.get('urls', [])
    if not urls:
        return web.json_response({'status': 'no urls supplied'}, status=400)

    method = body.get('method', 'GET')
    if not utils.is_valid_method(method):
        return web.json_response(
            {'status': f'{method} method is not a valid method.'},
             status=400
        )
    if not utils.is_supported(method):
        return web.json_response(
            {'status': f'{method} method is not supported. Supported methods: f{utils.get_supported()}'},
            status=405,
        )
    
    output = body.get('output', 'json')
    if not utils.is_supported_output(output):
        return web.json_response(
            {'status': f'{output} output format is not supported. Supported formats: f{utils.get_supported_outputs()}'},
            status=405,
        )

    max_age_days = body.get('max_age', 1)

    category = body.get('category', 'unspecified')

    data = DataHandler(
        db=db,
        urls=urls,
        method=method,
        output=output,
        max_age_days=max_age_days,
        category=category,
        proxy=proxy,
        headers=body.get('headers', {}),
        cookies=body.get('cookies', {}),
        timeout=15,
        logger=logger
    )

    await data.post_init()

    result, status = await data._process()

    if status != 200:
        return web.json_response({'status': 'fail', 'result': result}, status=status)
    else:
        return web.json_response(result, status=status)


@routes.post('/proxies/add')
async def add_proxies(request):
    body = await request.json()
    if body.get('proxies'):
        total_count = len(body.get('proxies'))
        inserted = await proxy.batch_add(body.get('proxies'))
        return web.json_response({
            'status': 'ok',
            'inserted': f'{inserted}',
            'total_in': f'{total_count}',
        }, status=200)
    return web.json_response({'status': 'ok'}, status=200)


@routes.get('/proxies/info')
async def get_proxies_info(request):
    return web.json_response({
        'total': await proxy.count,
        'working': await proxy.working
    })


@routes.get('/proxies/{amount:\d+}')
async def get_proxies(self, amount):
    return web.json_response({
        'proxies': await proxy.get_proxies(amount)
    })

app = web.Application()

app.add_routes(routes)

if __name__ == '__main__':
    web.run_app(app)