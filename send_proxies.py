import requests

with open('proxy-list.txt', 'r+') as f:
    proxies = []
    for line in f.readlines():
        line = line.strip()
        proxies.append(str(line))
    requests.post('http://127.0.0.1:8000/proxies/add', json={'proxies': proxies})
